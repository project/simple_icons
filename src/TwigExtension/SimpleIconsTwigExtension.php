<?php

namespace Drupal\simple_icons\TwigExtension;

use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

/**
 * Class SimpleIconsTwigExtension.
 */
class SimpleIconsTwigExtension extends AbstractExtension {

  /**
   * {@inheritdoc}
   */
  public function getName(): string {
    return 'simple_icons.twig.extension';
  }

  /**
   * {@inheritdoc}
   *
   * @return array
   *   An array of Twig functions.
   */
  public function getFunctions(): array {
    return [
      new TwigFunction('simple_icons_icon', [$this, 'simpleIconsIcon']),
    ];
  }

  /**
   * Returns markup for a Simple Icons icon.
   *
   * @param string $slug
   *   The Simple Icon slug of the icon you want to print.
   *
   * @return string
   *   Markup of the SVG
   */
  public function simpleIconsIcon($slug): string {
    $service = \Drupal::service('simple_icons.icon_markup');

    return $service->getIconMarkup($slug);
  }

}
