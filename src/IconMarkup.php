<?php

namespace Drupal\simple_icons;

use enshrined\svgSanitize\Sanitizer;
use Drupal\Component\Utility\Html;
use Symfony\Component\DomCrawler\Crawler;

/**
 * Manipulate and sanitise Simple Icons SVG and return in render array form.
 */
class IconMarkup {

  /**
   * Constructs a new IconMarkup object.
   */
  public function __construct() {

  }

  /**
   * Produce a render array to display a Simple Icons icon.
   *
   * @param string $slug
   *   The Simple Icons icon slug, e.g. `drupal`.
   *
   * @return array
   *   A Drupal render array.
   */
  public function getIconMarkup(string $slug) {
    if ($slug) {
      $markup = file_get_contents(DRUPAL_ROOT . '/libraries/simple-icons/icons/' . $slug . '.svg');
      $sanitized_markup = $this->sanitize($markup);
      $sanitized_markup = $this->removeWrapper($sanitized_markup);

      return [
        '#theme' => 'simple_icons_icon',
        '#svg_markup' => $sanitized_markup,
        '#slug' => $slug,
      ];

    }
  }

  /**
   * Remove the wrapping <svg> element provided by Simple Icons.
   *
   * This is so we can provide our own element via Twig. This way, users can
   * control classes etc on the <svg> element.
   *
   * @param string $markup
   *   SVG markup.
   *
   * @return string
   *   SVG markup.
   */
  public function removeWrapper(string $markup) {
    $dom = Html::load($markup);
    $crawler = new Crawler($dom);

    return $crawler->filter('svg')->html();
  }

  /**
   * Sanitizes contents of the SVG.
   *
   * This is in case Simple Icons publishes a SVG with malicious content.
   *
   * @param string $svg
   *   SVG contents.
   *
   * @return string
   *   Markup.
   */
  public function sanitize(string $svg) {
    $sanitizer = new Sanitizer();
    $sanitizer->removeRemoteReferences(TRUE);
    return $sanitizer->sanitize($svg);
  }

}
