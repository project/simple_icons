<?php

namespace Drupal\simple_icons\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;

/**
 * Plugin implementation of the 'simple_icons_icon' widget.
 *
 * @FieldWidget(
 *   id = "simple_icons_icon",
 *   module = "simple_icons",
 *   label = @Translation("Simple Icons icon"),
 *   field_types = {
 *     "simple_icons_icon"
 *   }
 * )
 */
class SimpleIconsIcon extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $icon_data = file_get_contents(\Drupal::service('extension.list.module')->getPath('simple_icons') . '/icon-data.json');
    $json_icon_data = Json::decode($icon_data);

    $element['value'] = $element + [
      '#type' => 'select',
      '#options' => [0 => $this->t('- Select icon -')] + $json_icon_data,
      '#default_value' => isset($items[$delta]->value) ? $items[$delta]->value : NULL,
      '#ajax' => [
        'callback' => [$this, 'simpleIconsAjaxPreviewCallback'],
        'event' => 'change',
        'progress' => [
          'type' => 'throbber',
          'message' => 'Loading preview…',
          'wrapper' => 'simple_icons_preview',
        ],
      ],
      '#suffix' => '<div class="' . implode('-', $element['#field_parents']) . '-simple-icons__icon-preview">' . $this->producePreviewMarkup(isset($items[$delta]->value) ? $items[$delta]->value : NULL) . '</div>',
      '#attached' => [
        'library' => ['simple_icons/icon_preview'],
      ],
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function simpleIconsAjaxPreviewCallback(&$form, &$form_state) {
    $response = new AjaxResponse();
    $element = $form_state->getTriggeringElement();
    $markup = $this->producePreviewMarkup($element['#value']);
    $response->addCommand(new HtmlCommand('.' . implode('-', $element['#field_parents']) . '-simple-icons__icon-preview', $markup));

    return $response;
  }

  /**
   * {@inheritdoc}
   */
  private function producePreviewMarkup($slug) {
    $markup = '&nbsp;';

    if ($slug) {
      $markup = '<img height="32" width="32" src="' . base_path() . 'libraries/simple-icons/icons/' . $slug . '.svg" />';
    }

    return $markup;
  }

}
