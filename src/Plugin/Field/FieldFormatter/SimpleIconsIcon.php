<?php

namespace Drupal\simple_icons\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\simple_icons\IconMarkup;

/**
 * Plugin implementation of the 'simple_icons_icon' formatter.
 *
 * @FieldFormatter(
 *   id = "simple_icons_icon",
 *   label = @Translation("Simple Icons icon"),
 *   field_types = {
 *     "simple_icons_icon"
 *   }
 * )
 */
class SimpleIconsIcon extends FormatterBase implements ContainerFactoryPluginInterface {


  /**
   * Drupal\simple_icons\IconMarkup definition.
   *
   * @var \Drupal\simple_icons\IconMarkup
   */
  protected $iconMarkup;

  /**
   * Construct a DocumentDisplayFieldFormatter.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Any third party settings settings.
   * @param \Drupal\simple_icons\IconMarkup $iconMarkup
   *   The icon markup service.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, IconMarkup $iconMarkup) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings, $iconMarkup);
    $this->iconMarkup = $iconMarkup;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('simple_icons.icon_markup')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    foreach ($items as $delta => $item) {
      $elements[$delta] = $this->viewValue($item);
    }

    return $elements;
  }

  /**
   * Generate the output appropriate for one field item.
   *
   * @param \Drupal\Core\Field\FieldItemInterface $item
   *   One field item.
   *
   * @return array
   *   The textual output generated.
   */
  protected function viewValue(FieldItemInterface $item) {
    return $this->iconMarkup->getIconMarkup($item->value);
  }

}
