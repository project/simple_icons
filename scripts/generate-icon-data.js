const simpleIcons = require('simple-icons');
const _ = require('lodash');
const fs = require('fs')

const storeData = (data, path) => {
    try {
        fs.writeFileSync(path, JSON.stringify(data, null, 4))
    } catch (err) {
        console.error(err)
    }
};

let iconData = {};

_.map(
    simpleIcons, function (object) {
        const data = _.pick(object, ['slug', 'title']);
        iconData[data.slug] = data.title;
    }
);


storeData(iconData, 'icon-data.json');
