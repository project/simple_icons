## INTRODUCTION

Simple Icons defines a field that allows users to add brand icons from the
[Simple Icons](https://github.com/simple-icons/simple-icons) project.

The markup is outputted as raw SVG markup so it can be easily themed, but it is
sanitised before being output for security.

- For a full description of the module, visit the
  [project page](https://www.drupal.org/project/simple_icons)
- To submit bug reports and feature suggestions, or track changes, visit the
  [project's issue page](https://www.drupal.org/project/issues/simple_icons)

## REQUIREMENTS

This module requires that
[Simple Icons](https://github.com/simple-icons/simple-icons) is made available
in your Drupal site's `./libraries` directory. Instructions on how to do this
can be found on the [project page](https://www.drupal.org/project/simple_icons).

[`enshrined/svg-sanitize`](https://packagist.org/packages/enshrined/svg-sanitize)
is installed via Composer to try and sanitise the SVG provided by the
[Simple Icons](https://github.com/simple-icons/simple-icons) project, in case
malicious code is ever added to the Simple Icons SVGs.

## INSTALLATION

- Once the Simple Icons library is in the correct location (see Requirements)
  then you can install this module as normal. Visit
  https://www.drupal.org/node/895232 for further information.

- After installation you will have a 'Simple Icons icon' field type and field
  formatter available, as well as a Twig function called `simple_icons_icon`
  that accepts one parameter, a string representing a Simple Icons slug, for
  example `simple_icons_icon('drupal')` would output the Drupal Association
  logo.

## CONFIGURATION

There is currently no need for configuration.

## THEMING

Need to add an CSS class to the icons provided by this module? Override the
simple-icons-icon.html.twig template provided by this module and you can edit
the markup of the `<svg>` element that wraps the icons.

## ACCESSIBILITY

A `<title>` element is contained within each SVG with the name of the brand that
is represented by the icon.
